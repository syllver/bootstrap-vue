export default {
    // inventory
    
    inventoryCounter: state => {
        var count = 0;
        state.inventoryList.map(inventoryList => {
            count += parseInt(inventoryList.quantity)
        })
        return count;
    },

    salesOr(state){
        console.log(state.salesList[state.salesList.length-1]);

        if (state.salesList[state.salesList.length-1]) {
         return state.salesList[state.salesList.length-1].or_no+1
        }
    },

    deliveryCounter: state => {
        var count = 0;
        state.deliveryList.map(delivery => {
            count += deliveryList.quantity
        })
        return count;
    },
    delivery_transactions: state => state.delivery_transactions,
    // suppliers
    suppliers: state => {
        return state.suppliers
    },

    supplierCounter(state) {
        return state.suppliersList.length
    },

    salesCounter: state => {
        return sales.length;
    },

    // zero stocks
    zeroStocks: state => {
        var j = 0;
        state.inventoryList.map(inventoryList => {
            if(inventoryList.quantity == 0){
                j += 1;
            }
        })
        // console.log(j)
        return j
    },

    lowStocks: state => {
        var low = [];
        for(var i = 0; i < state.inventoryList.length; i++){
            if(state.inventoryList[i].quantity <= 35){
                low.push(state.inventoryList[i])
            }
        }
        return low
    },


    // =====================================================


    //roles
    rolesList(state) {
        return state.roles;
    },

    userList(state) {
        return state.users;
    },

    inventoryList(state) {
        return state.inventory;
    },

    inventoryCounter: state => {
        var count = 0;
        state.inventory.map(inventory => {
            count += inventory.qty
        })
        return count;
    },

    suppliersList(state) {
        return state.suppliers;
    },

    supplierCounter: (state, suppliers) => {
        return state.suppliers.length
    },

    deliveryList(state) {
        return state.delivery_transactions;
    },

    salesList(state) {
        return state.salesList;
    },

    getSelectedTransaction: state => {
        return state.selectedTransaction;
    },

    salesList(state){
        console.log('getter: ',state.salesList);
        return state.sales_transactions;
    },

    getDateFilteredDelivery(state) {
        return state.delivery_datefilter;
    },

    getSelectedRole: state => {
        console.log('@here again: ', state.selectedRole)
        return state.selectedRole;
    },

    getSelectedItem(state) {
        return state.selectedItem;
    },


}